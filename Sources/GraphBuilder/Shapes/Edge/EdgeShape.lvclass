﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">-----------------------------------------------------------------------
BSD 3-Clause License

Copyright (c) 2020, Cyril GAMBINI
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"#;5F.31QU+!!.-6E.$4%*76Q!!$+1!!!0W!!!!)!!!$)1!!!!J!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)2272H:6.I98"F,GRW9WRB=X-!!!!!!!#A&amp;!#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!0I&gt;LTF2\V*,NP5.,T2V##M!!!!-!!!!%!!!!!#L^^";CB'`2:OJS@L-/*J=V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!K5`#NT8.A5#/)O"U&gt;B-H*A%!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!"$H9QD1CX[J)6T7WCL2:&amp;RJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!&amp;A!!!!JYH'.A:G!39""A&amp;'!!!!$W!$=!!!!!!"]!!!$-?*RD9-!0`A-"!35U"Q4=Q!T%,%$-#A""[1A&amp;!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!!-&amp;!#!!!!!"$%U,D!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Q5!)!!!!!%-41O-!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$"1!A!!!!!1R.#YQ!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!,GZ!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!,H2R=P2O1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!,H2R&lt;_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!!!!``]!!,H2R&lt;_`P\_`P\_`S^'Z!!!!!!!!!!!!!!!!!!$``Q$,R&lt;_`P\_`P\_`P\_`P]P2!!!!!!!!!!!!!!!!!0``!-8&amp;P\_`P\_`P\_`P\_``]M!!!!!!!!!!!!!!!!!``]!R=P,R&lt;_`P\_`P\_`````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]7`P\_````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P&amp;U@```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-P,S]P,S]P,````````S]M!!!!!!!!!!!!!!!!!``]!!-8&amp;S]P,S]P`````S^(&amp;!!!!!!!!!!!!!!!!!!$``Q!!!!$&amp;S]P,S```S]P&amp;!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!R=P,S]O`!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!-7`!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!#!!%!!!!!!!Q!!5:13&amp;!!!!!!!!-!!!*`!!!%[8C=L:2.;"."&amp;-@@B+V-1IOTM&lt;6&gt;;%AMWVD%A#BK$8Y5/R7KJ3CRY#(5",&gt;_1.NI0M24?VG%((IRG)017QBY]B#+&gt;QF?^G"0#P91'`$OJ3D9T@JWUNV]&amp;/0&amp;82C7:8\P`_&lt;^`QS!^*W.?"KQ:A*B?`AR:Y*0-QB!.5,BY!G`",:!@A-:6)A*5X3"\8A;:.3%@MU)UT-]$T^QN`8.?A7P#7'\O05)5\#9TY3DGD%MXV2L4(UXKO&lt;\H+IS".A';8DOK='@&gt;%.0I3$I%`9K2UA$#$]B3@L9\?4KEK\;@\U2KIC38B-9.Q&lt;3;OUE6E4J$[)E791334EF!5N/1+63;5&amp;S%QK,.C:NJI2FN]BC$]&lt;0D5"'L:U7D%]QK,0F[0"AIW$X&lt;E0&gt;[$&amp;O-%32?_%=O?2I];!Z*&lt;B[P9Y=LA@=9R-'V&gt;JVKN"&gt;\7P@Q"X^02!AV@P5_G+^N8FZXH:"\07D%?.2QG,Y(40BF'ZY%ERSN(*1%$:)DAW8&lt;2NO#"O]0-_AZ50B(TZQK4I7G&amp;\/:&lt;*,[6$K9?D"=D+4#4V.0XG?T#[&amp;N'1W?&gt;CB+^R1TNGHN]6%/'!)*(A'C@:JJ["=,O-!='WB6R%&gt;6GMO*YP4;/\%N&gt;&lt;%&lt;&gt;87Z+\BZ0CPOY`M[9V(05ZGI:87MZB7G85XV*(7]`]`L2=Q2'N&gt;;95YPCM^EH?R#&lt;7F.9\5#M2\-*0)L(?E6;AY/LX3?OFQ7O//FJP79L(9Q?(-IWZ;*5+;;&gt;7XL8VL(T/,YZ5A!1G27@$/NX6/&gt;T\GX-O&amp;RF2O5?8P'^A-CKW&lt;--3H`:`9,&amp;&lt;LJ\.UBGWCKXA&gt;(;?&lt;&gt;+`[W&lt;V_KH-OK7`$+LGFPAH3E4_6`7=Q!!!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!&lt;A!!!!'!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B1!A!!!!!!"!!A!-0````]!!1!!!!!!)!!!!!%!'%"1!!!2272H:6.I98"F,GRW9WRB=X-!!1!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!.L^6&gt;-!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$;`684!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R1!A!!!!!!"!!A!-0````]!!1!!!!!!)!!!!!%!'%"1!!!2272H:6.I98"F,GRW9WRB=X-!!1!!!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G55!)!!!!!!!1!&amp;!!-!!!%!!!!!!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S&amp;!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!!!!!!!!!!!!1!!A!&amp;!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)U!!!#\?*RD9'"A:UBG_(DL&lt;_BF"A9':C(XIM3#$+@3T*S5V#+^H,+=T#2"VZ4UV/#-R)*5)$]Z*\'YG"=BEFS3Q[$B%-$!)/O=5VJ=EFKEE*_G!&amp;;E5&amp;#57::9EKK1EFC3S-!+B!R=$#E-$)Q@'"C"('9IF]%9S'1"UCJ%G-)'6-=%6-U+:,%"!#JJ-*)!!!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W1$5!!!!51!0"!!!!!!0!.E!V!!!!&amp;I!$Q1!!!!!$Q$:!.1!!!"DA!#%!)!!!!]!W1$5#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4)!!!"35V*$$1I!!UR71U.-1F:8!!!-J!!!!`9!!!!A!!!-B!!!!!!!!!!!!!!!)!!!!$1!!!0I!!!!'ER*1EY!!!!!!!!"3%R75V)!!!!!!!!"8&amp;*55U=!!!!!!!!"=%.$5V1!!!!!!!!"B%R*&gt;GE!!!!!!!!"G%.04F!!!!!!!!!"L&amp;2./$!!!!!!!!!"Q%2'2&amp;-!!!!!!!!"V%R*:(-!!!!!!!!"[(:F=H-!!!!%!!!"`%&gt;$5&amp;)!!!!!!!!#9%F$4UY!!!!!!!!#&gt;'FD&lt;$A!!!!!!!!#C%.11T)!!!!!!!!#H%R*:H!!!!!!!!!#M%:13')!!!!!!!!#R%:15U5!!!!!!!!#W&amp;:12&amp;!!!!!!!!!#\%R*9G1!!!!!!!!$!%*%3')!!!!!!!!$&amp;%*%5U5!!!!!!!!$+&amp;:*6&amp;-!!!!!!!!$0%253&amp;!!!!!!!!!$5%V6351!!!!!!!!$:%B*5V1!!!!!!!!$?&amp;:$6&amp;!!!!!!!!!$D%:515)!!!!!!!!$I!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!%=!!!!!!!!!!$`````!!!!!!!!!31!!!!!!!!!!0````]!!!!!!!!"1!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!4`````!!!!!!!!!81!!!!!!!!!"`````]!!!!!!!!"B!!!!!!!!!!)`````Q!!!!!!!!'5!!!!!!!!!!H`````!!!!!!!!!;1!!!!!!!!!#P````]!!!!!!!!"N!!!!!!!!!!!`````Q!!!!!!!!(%!!!!!!!!!!$`````!!!!!!!!!&gt;A!!!!!!!!!!0````]!!!!!!!!#8!!!!!!!!!!!`````Q!!!!!!!!:A!!!!!!!!!!$`````!!!!!!!!"GA!!!!!!!!!!0````]!!!!!!!!'?!!!!!!!!!!!`````Q!!!!!!!!D]!!!!!!!!!!$`````!!!!!!!!#11!!!!!!!!!!0````]!!!!!!!!*$!!!!!!!!!!!`````Q!!!!!!!!E=!!!!!!!!!!$`````!!!!!!!!#91!!!!!!!!!!0````]!!!!!!!!*D!!!!!!!!!!!`````Q!!!!!!!!N)!!!!!!!!!!$`````!!!!!!!!#V!!!!!!!!!!!0````]!!!!!!!!,7!!!!!!!!!!!`````Q!!!!!!!!O%!!!!!!!!!)$`````!!!!!!!!$"A!!!!!$56E:W64;'&amp;Q:3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)2272H:6.I98"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;&amp;:'&gt;F5WBB='5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;1#!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="EdgeShape.ctl" Type="Class Private Data" URL="EdgeShape.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>

﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">-----------------------------------------------------------------------
BSD 3-Clause License

Copyright (c) 2020, Cyril GAMBINI
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"#;5F.31QU+!!.-6E.$4%*76Q!!$+1!!!0W!!!!)!!!$)1!!!!J!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)24G^E:6.I98"F,GRW9WRB=X-!!!!!!!#A&amp;!#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)^?F..)1:^"P4(A@1&amp;0NBQ!!!!-!!!!%!!!!!$'\KD&gt;WV.-4,8FYU@F\PC+V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!\*F5O]5BUEGA4O#Q2I1]E!%!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!"$H9QD1CX[J)6T7WCL2:&amp;RJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!&amp;A!!!!JYH'.A:G!39""A&amp;'!!!!$W!$=!!!!!!"]!!!$-?*RD9-!0`A-"!35U"Q4=Q!T%,%$-#A""[1A&amp;!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!!-&amp;!#!!!!!"$%U,D!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Q5!)!!!!!%-41O-!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$"1!A!!!!!1R.#YQ!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!"56!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!"7M:)CM&amp;1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!"7M:%"!1%#)L"5!!!!!!!!!!!!!!!!!!!!!``]!!"7M:%"!1%"!1%"!C+Q6!!!!!!!!!!!!!!!!!!$``Q#):%"!1%"!1%"!1%"!1)CM!!!!!!!!!!!!!!!!!0``!'2E1%"!1%"!1%"!1%"!`YA!!!!!!!!!!!!!!!!!``]!:)C):%"!1%"!1%"!````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C'2!1%"!``````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)BEL0```````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!)C)C)C)C)C)````````C)A!!!!!!!!!!!!!!!!!``]!!'2EC)C)C)D`````C+RE!!!!!!!!!!!!!!!!!!$``Q!!!!"EC)C)C0``C)BE!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!:)C)C)B!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!'2!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!#!!%!!!!!!!Q!!5:13&amp;!!!!!!!!-!!!+!!!!%[8C=L:2.;"."&amp;-@@B+V-1IOTM&lt;6&gt;;5AMWVD%ACBK$8Y5/R5KJ;CRY#(5"$&gt;_1'MU(_+JP3R#$LU9T%(I,11]?1D&amp;OQ1P?\!H"8O)$8DX5B4M:HU\[7Y_CP(C,AT,-L`X@`0_@Q:!_MZ'0!V9.9'Q8@S9.]'H'13A/EFB`QG`",:)@A-:6)A*UX32&lt;8M;:.3%@MU)U^-]$T^QN`8.?A7P#7%\O0515\#9TY4$GD%MXV"L4(UXKO&lt;\H+IS".A[;8DOKM'@&gt;&amp;V0I3$I%`9K4Z)'%(Z=EP3RGYEH36WV`XIHK3*+?EVAX"B)K\547"'F0YC3:!F+*/75"#QZ!:6+J18*43AMWJCSG2+7X32,02A`.Q):N8:+-$\"I-[GI]/$D9,&gt;OQVVIU?YQ2"&amp;\I6TZ*+DR90GN/$K^4JSO/ZTDUQ96'P8K%*XN+^^!\@U^U#!6/^2[YPVVO&lt;F"&gt;M&amp;M&gt;?02IR(#)PC&gt;^3%E\LBC40*U=J"1&gt;AA/4:=MGWY,GTQ]DS$FA_&amp;@`D!J?J99'9ZF]EGU[(5A^$^Z51G%XK;@PQ]E5W'N%1W=&gt;#BS^R1TNKHN]6%/'!)*(A']@:JJ["=,O-!='WB6R!&gt;6GMO*YP4;/\%N&gt;&lt;%&lt;&gt;87Z+\CZ0CP/Q`N[9V(0%ZGI:87-ZB7G85XV*(7=`]`L?=R2+N&gt;;959PCM^EH?B#&lt;7F.9&lt;5#M2[-&amp;0)L(7E6;AY/LX3?P&amp;A7G//FJP79L(9Q?(-)WZ;*5+;;&gt;7XL$VL$T/,YZ5A$H'27@!ON(6/NT`GX-O&amp;2F6O5?8P'^AMCKW:--2H`*`9(&amp;&lt;LJX.UFGWAKXA&gt;(;5&lt;&gt;,@[W&lt;V_KP-OK7`"&lt;8*-@2/E)X]!E`&gt;GYQ!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!&lt;A!!!!'!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B1!A!!!!!!"!!A!-0````]!!1!!!!!!)!!!!!%!'%"1!!!24G^E:6.I98"F,GRW9WRB=X-!!1!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!.L^6?5!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$;`68F!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R1!A!!!!!!"!!A!-0````]!!1!!!!!!)!!!!!%!'%"1!!!24G^E:6.I98"F,GRW9WRB=X-!!1!!!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G55!)!!!!!!!1!&amp;!!-!!!%!!!!!!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S&amp;!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!!!!!!!!!!!!1!!A!&amp;!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)U!!!#\?*RD9'"A:UBG_(DL&lt;_B4"A9':C(XIM3#$+@3T*S5V#+^H,+=T#2"P`S5V/#-R)*5)$]Z*\'YG"=BEFS3Q[$B%-$!)/O=5VJ=EFKEE*_G!&amp;;E5&amp;#57::9EKK1EFC3S-!+B!R=$#E-$)Q@'"C"('9IF]%9S'1"UCJ%G-)'6-=%6-U+:,%"!%G&lt;--9!!!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W1$5!!!!51!0"!!!!!!0!.E!V!!!!&amp;I!$Q1!!!!!$Q$:!.1!!!"DA!#%!)!!!!]!W1$5#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4)!!!"35V*$$1I!!UR71U.-1F:8!!!-J!!!!`9!!!!A!!!-B!!!!!!!!!!!!!!!)!!!!$1!!!0I!!!!'ER*1EY!!!!!!!!"3%R75V)!!!!!!!!"8&amp;*55U=!!!!!!!!"=%.$5V1!!!!!!!!"B%R*&gt;GE!!!!!!!!"G%.04F!!!!!!!!!"L&amp;2./$!!!!!!!!!"Q%2'2&amp;-!!!!!!!!"V%R*:(-!!!!!!!!"[(:F=H-!!!!%!!!"`%&gt;$5&amp;)!!!!!!!!#9%F$4UY!!!!!!!!#&gt;'FD&lt;$A!!!!!!!!#C%.11T)!!!!!!!!#H%R*:H!!!!!!!!!#M%:13')!!!!!!!!#R%:15U5!!!!!!!!#W&amp;:12&amp;!!!!!!!!!#\%R*9G1!!!!!!!!$!%*%3')!!!!!!!!$&amp;%*%5U5!!!!!!!!$+&amp;:*6&amp;-!!!!!!!!$0%253&amp;!!!!!!!!!$5%V6351!!!!!!!!$:%B*5V1!!!!!!!!$?&amp;:$6&amp;!!!!!!!!!$D%:515)!!!!!!!!$I!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!%=!!!!!!!!!!$`````!!!!!!!!!31!!!!!!!!!!0````]!!!!!!!!"1!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!4`````!!!!!!!!!81!!!!!!!!!"`````]!!!!!!!!"B!!!!!!!!!!)`````Q!!!!!!!!'5!!!!!!!!!!H`````!!!!!!!!!;1!!!!!!!!!#P````]!!!!!!!!"N!!!!!!!!!!!`````Q!!!!!!!!(%!!!!!!!!!!$`````!!!!!!!!!&gt;A!!!!!!!!!!0````]!!!!!!!!#8!!!!!!!!!!!`````Q!!!!!!!!:A!!!!!!!!!!$`````!!!!!!!!"GA!!!!!!!!!!0````]!!!!!!!!'?!!!!!!!!!!!`````Q!!!!!!!!D]!!!!!!!!!!$`````!!!!!!!!#11!!!!!!!!!!0````]!!!!!!!!*$!!!!!!!!!!!`````Q!!!!!!!!E=!!!!!!!!!!$`````!!!!!!!!#91!!!!!!!!!!0````]!!!!!!!!*D!!!!!!!!!!!`````Q!!!!!!!!N)!!!!!!!!!!$`````!!!!!!!!#V!!!!!!!!!!!0````]!!!!!!!!,7!!!!!!!!!!!`````Q!!!!!!!!O%!!!!!!!!!)$`````!!!!!!!!$"A!!!!!$5ZP:'64;'&amp;Q:3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)24G^E:6.I98"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;/&lt;W2F5WBB='5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;1#!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="NodeShape.ctl" Type="Class Private Data" URL="NodeShape.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>

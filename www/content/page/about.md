---
title: About this project
subtitle: AntiDoc is the cure to your LabVIEW project documentation
comments: true
---

This project was born in mid-2019 to help LabVIEW developers to improve documentation provided with their projects.


### Goal

Use LabVIEW content to generate valuable documentation that will help maintain your projects and onboard new resources in the development team.

### Project Maintainer

[Olivier Jourdan](https://www.linkedin.com/in/jourdanolivier/)

### Project Contributors
[Bertrand Pouteau](https://www.linkedin.com/in/bertrand-pouteau-5717028/)

[Cyril Gambini](https://www.linkedin.com/in/cyrilgambini/)

[Fabiola Delacueva](https://www.linkedin.com/in/fabioladelacueva/)

[Joerg Hampel](https://www.linkedin.com/in/joerghampel/)

[Tatiana Boyé](https://www.linkedin.com/in/tatiana-boye/)
